#ifndef __Zip__Zip__
#define __Zip__Zip__

//
//  Zip.h
//  Zip
//
//  Created by Nathan Wehr on 9/16/14.
//  Copyright (c) 2014 EvriChart, Inc. All rights reserved.
//

// C
#include <zlib.h>

// C++
#include <string>

// Boost
#include <boost/property_tree/ptree.hpp>

// Eloquent
#include "Eloquent/Extensions/Filters/Filter.h"
#include "ZipAlgorithm.h"

namespace Eloquent {
	///////////////////////////////////////////////////////////////////////////////
	// Zip : Filter
	///////////////////////////////////////////////////////////////////////////////
	class Zip : public Filter {
	public:
		Zip( const boost::property_tree::ptree::value_type& i_Config );
		virtual ~Zip();
		
		virtual bool operator<<( std::string& io_Data );
		
	protected:
		boost::optional<bool> m_Compress;
		boost::optional<bool> m_Decompress;
		
	};
	
}

#endif /* defined(__Zip__Zip__) */
