//
//  Zip.cpp
//  Zip
//
//  Created by Nathan Wehr on 9/16/14.
//  Copyright (c) 2014 EvriChart, Inc. All rights reserved.
//

#include "Zip.h"

// C
#include <syslog.h>

// C++
#include <sstream>
#include <iostream>

// Boost
//#include <boost/iostreams/filtering_streambuf.hpp>
//#include <boost/iostreams/copy.hpp>
//#include <boost/iostreams/filter/gzip.hpp>

Eloquent::Zip::Zip( const boost::property_tree::ptree::value_type& i_Config )
: Filter( i_Config )
, m_Compress( m_Config.second.get_optional<bool>( "compress" ) )
, m_Decompress( m_Config.second.get_optional<bool>( "decompress" ) )
{}

Eloquent::Zip::~Zip() {}
	
bool Eloquent::Zip::operator<<( std::string& io_Data ) {
	try {
//		std::stringstream Source( io_Data );
//		std::stringstream Sink;
//		
//		boost::iostreams::filtering_streambuf<boost::iostreams::input> in;
//		
//		if( m_Decompress.is_initialized() && m_Decompress.get() == true ) {
//			in.push( boost::iostreams::gzip_decompressor() );
//		} else {
//			in.push( boost::iostreams::gzip_compressor() );
//		}
//		
//		in.push( Source );
//		
//		boost::iostreams::copy( in, Sink );
//		
//		io_Data = Sink.str();
//		
//		return Continue( true );
		
		if( m_Decompress.is_initialized() && m_Decompress.get() == true ) {
			io_Data = decompress_string( io_Data );
			return Continue( true );
		}
		
		io_Data = compress_string( io_Data );
		
		return Continue( true );
		
	} catch( std::exception& e ) {
		syslog( LOG_ERR, "%s #Zip::operator<<()", e.what() );
		
		return Continue( false );
		
	}
	
}
