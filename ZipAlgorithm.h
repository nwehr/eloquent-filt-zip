//
//  ZipAlgorithm.h
//  Zip
//
//  Created by Nathan Wehr on 9/17/14.
//  Copyright (c) 2014 EvriChart, Inc. All rights reserved.
//

#ifndef __Zip__ZipAlgorithm__
#define __Zip__ZipAlgorithm__

// C
#include <zconf.h>
#include <zlib.h>

// C++
#include <string>
#include <sstream>
#include <exception>

std::string compress_string( const std::string& str, int compressionlevel = Z_BEST_COMPRESSION );
std::string decompress_string(const std::string& str);

#endif /* defined(__Zip__ZipAlgorithm__) */
