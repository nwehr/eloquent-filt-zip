//
//  ZipFactory.cpp
//  Zip
//
//  Created by Nathan Wehr on 9/16/14.
//  Copyright (c) 2014 EvriChart, Inc. All rights reserved.
//

#include "ZipFactory.h"

Eloquent::ZipFactory::ZipFactory() : FilterFactory() {}
Eloquent::ZipFactory::~ZipFactory() {}
	
Eloquent::Filter* Eloquent::ZipFactory::New( const boost::property_tree::ptree::value_type& i_ConfigNode ) {
	return new Zip( i_ConfigNode );
}
