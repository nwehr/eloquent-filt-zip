//
//  ZipAttach.cpp
//  Zip
//
//  Created by Nathan Wehr on 9/16/14.
//  Copyright (c) 2014 EvriChart, Inc. All rights reserved.
//

#include "ZipAttach.h"
#include "ZipFactory.h"

extern "C" void* Attach(void) {
	return new Eloquent::ZipFactory();
}