#ifndef __Zip__ZipFactory__
#define __Zip__ZipFactory__

//
//  ZipFactory.h
//  Zip
//
//  Created by Nathan Wehr on 9/16/14.
//  Copyright (c) 2014 EvriChart, Inc. All rights reserved.
//


#include "Eloquent/Extensions/Filters/FilterFactory.h"
#include "Zip.h"

namespace Eloquent {
	///////////////////////////////////////////////////////////////////////////////
	// ZipFactory : FilterFactory
	///////////////////////////////////////////////////////////////////////////////
	class ZipFactory : public FilterFactory {
	public:
		ZipFactory(); 
		virtual ~ZipFactory();
		
		virtual Filter* New( const boost::property_tree::ptree::value_type& i_ConfigNode );
		
	};
	
}

#endif /* defined(__Zip__ZipFactory__) */
